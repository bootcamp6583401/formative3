public abstract class MoviePlayer {
    public final void play() {
        prolouge();
        climax();
        epilogue();
    }

    protected abstract void prolouge();

    protected abstract void climax();

    protected abstract void epilogue();
}