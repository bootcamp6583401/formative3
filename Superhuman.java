public class Superhuman {
    private int powerLevel;

    public Superhuman(int powerLevel) {
        this.setPowerLevel(powerLevel);
    }

    public int getPowerLevel() {
        return powerLevel;
    }

    public void setPowerLevel(int powerLevel) {
        this.powerLevel = powerLevel;
    }

    public void sayHi() {
        System.out.println(String.format("Power leve: ", powerLevel));
    }
}
