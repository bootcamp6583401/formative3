/**
 * Animal
 */
public interface Animal {

    String description();
}