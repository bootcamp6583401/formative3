public class SuperFactory {
    public static Superhuman newSuper(String type, int powerLevel) {
        if (type.equals("hero")) {
            return new Superhero(powerLevel);
        } else if (type.equals("villain")) {
            return new Supervillain(powerLevel);
        }
        return null;
    }
}
