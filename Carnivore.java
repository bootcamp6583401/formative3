public class Carnivore extends AnimalDecorator {
    public Carnivore(Animal animal) {
        super(animal);
    }

    @Override
    public String description() {
        return animal.description() + " Nomnomonon!";
    }

}
