import java.util.ArrayList;

public class SingletonMovieDatabase {
    private static SingletonMovieDatabase instance;
    private static ArrayList<String> movies;

    private SingletonMovieDatabase() {
        movies = new ArrayList<>();
        System.out.println("Initializing Movie Database...");
    }

    public static SingletonMovieDatabase getInstance() {
        if (instance == null) {
            instance = new SingletonMovieDatabase();
        }
        return instance;
    }

    public void addMovie(String title, String genre) {
        movies.add(String.format("%s - %s", title, genre));
        System.out.println("Adding movie: " + title + " (" + genre + ")");
    }

    public void listMovies() {
        movies.forEach(movie -> System.out.println(movie));
    }

}
