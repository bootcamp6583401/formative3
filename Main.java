public class Main {
    public static void main(String[] args) {

        // Formative 1 - Singleton
        /*
         * Memastikan hanya satu instansi yang dapat diadakan. Jika instansi sudah ada
         * maka return, jika belum maka buat dan return
         */
        SingletonMovieDatabase movieDB = SingletonMovieDatabase.getInstance();

        SingletonMovieDatabase movieDB2 = SingletonMovieDatabase.getInstance();

        movieDB.addMovie("Shawshank Redemption", "Drama");

        movieDB2.addMovie("Avengers", "Superhero");

        movieDB.listMovies();

        // Formative 2 - Encapsulation
        /*
         * Membuat factory yang dapat menghasilkan kelas bermacam-macam dengan parent
         * yang sama
         */
        Superhero superman = (Superhero) SuperFactory.newSuper("hero", 10000);
        Supervillain joker = (Supervillain) SuperFactory.newSuper("villain", 500);

        superman.sayHi();
        joker.sayHi();

        // Formative 3 - Decorator
        /*
         * Pattern ini digunakan sebagai alternative inheritance yang dapat membuat kode
         * banyak dan susah dibaca.
         * Dengan ini kita dapat extend kelas kelas dengan memasukkannya ke dalam
         * wrapper yang menambahkan fitur-fitur
         */
        Animal eagle = new Carnivore(new Flight(new BaldEagle()));
        Animal toucan = new Herbivore(new Flight(new Toucan()));

        System.out.println(eagle.description());
        System.out.println(toucan.description());

        // Formative 4 - Template
        /*
         * Dengan design pattern ini kita bisa mendesign secara high-level metode-metode
         * yang akan di-implementasikan suatu kelas
         */
        HorrorMovie halloween = new HorrorMovie();
        halloween.play();
        DramaMovie notebook = new DramaMovie();
        notebook.play();
    }

}
