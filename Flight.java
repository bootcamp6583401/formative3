public class Flight extends AnimalDecorator {
    public Flight(Animal animal) {
        super(animal);
    }

    @Override
    public String description() {
        return animal.description() + " Whoooosh!";
    }

}
