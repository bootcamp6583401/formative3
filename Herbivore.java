public class Herbivore extends AnimalDecorator {
    public Herbivore(Animal animal) {
        super(animal);
    }

    @Override
    public String description() {
        return animal.description() + " Cupcupucpu!";
    }

}
